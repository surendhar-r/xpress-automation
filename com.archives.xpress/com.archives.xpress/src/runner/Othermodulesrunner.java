package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "feature", glue = "othermodules", plugin = { "pretty", "html:target/cucumber-reports" ,"junit:reports/cucumber.xml"})

public class Othermodulesrunner {
	
	

}
