package search;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Search {
	
@SuppressWarnings("unused")
	
	public static WebDriver driver;

	public String keyname;
	public String keyxpath;

	public String sorttype;
	public String sortxpath;

	public String reccount;
	public String recxpath;

	public String allignname;
	public String allignxpath;

	public String dateon;
	public String dateonxpath;
	public String dateendpart;
	public String Finalonxpath;
	public String dateyearxpath;
	public String dateweekxpath;
	public String datemonthxpath;
	public String datedayxpath;
	
	
	
	@Given("^Load the site \"([^\"]*)\"$")
	public void load_the_site(String arg1) throws Exception {
	    
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(arg1);
	    driver.manage().window().maximize();
	    Thread.sleep(5000);
		
	}

	@When("^id = \"([^\"]*)\"$")
	public void id(String arg1) throws Exception {
	    
		driver.findElement(By.xpath("//a[text()='Sign In']")).click();
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
	    driver.findElement(By.xpath("//input[@name='email']")).sendKeys(arg1);
	    
	}

	@When("^pwd = \"([^\"]*)\"$")
	public void pwd(String arg1) throws Exception {
	    
		driver.findElement(By.id("icon_telephone")).sendKeys(arg1);
		Thread.sleep(2000);
	}

	@Then("^Login-Validate-Process$")
	public void login_Validate_Process() throws Exception {
	    
		driver.findElement(By.xpath("//div[@class='row margin-altersignin']//button[@type='submit']")).click();
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
	    
	}

	@When("^Search-value = \"([^\"]*)\"$")
	public void search_value(String arg1) throws Exception {
	    
		driver.findElement(By.id("searchParameter")).clear();
	    driver.findElement(By.id("searchParameter")).sendKeys(arg1);
		Thread.sleep(2000);
	}

	@Then("^Click home page search button$")
	public void click_home_page_search_button() throws Exception {
	    
		driver.findElement(By.xpath("//form[@id='submit-products-search']//i[@class='material-icons']")).click();
		Thread.sleep(2000);
	}

	@When("^Top-Search-value = \"([^\"]*)\"$")
	public void top_Search_value(String arg1) throws Exception {
	    
		driver.findElement(By.id("searchTopParameter")).clear();
	    driver.findElement(By.id("searchTopParameter")).sendKeys(arg1);
	    Thread.sleep(4000);
	    
	}

	@Then("^Click top search button$")
	public void click_top_search_button() throws Exception {
		
		driver.findElement(By.xpath("//i[text()='search']")).click();
		Thread.sleep(4000);
	    
	}

	@Then("^Invalid Search message$")
	public void invalid_Search_message() throws Exception {
	    
		//Invalid Search Content
		
		driver.findElement(By.xpath("html/body/section/article/div/div[2]/div[2]/div/div/span/p/a[2]")).click(); //Click Here
		Thread.sleep(4000);
		
		driver.findElement(By.xpath("//*[@id=\"contact_form\"]/div/div[9]/textarea")).sendKeys("Testing please Ignore");
		Thread.sleep(2000);
		
		//Submit Button
	    driver.findElement(By.xpath("html/body/div[10]/div/div[3]/form/div/div[11]/div/button")).click(); // Submit Button
	    Thread.sleep(2000);
	    
	    driver.switchTo().alert().accept();
	    Thread.sleep(4000);
	    
	    driver.findElement(By.xpath("html/body/div[10]/div/div[1]/i")).click(); //Close Button
	    Thread.sleep(2000);
	    
	    
	}

	@Given("^Select Advanced-Search$")
	public void select_Advanced_Search() throws Exception {
	    
		driver.findElement(By.xpath("//h5[text()='Advanced Search']")).click();
		Thread.sleep(4000);
	    
	}

	@When("^Adv-Search-All = \"([^\"]*)\"$")
	public void adv_Search_All(String arg1) throws Exception {
	    
		driver.findElement(By.id("searchTopParameter")).clear();
		driver.findElement(By.id("allSearchField")).clear();
		Thread.sleep(2000); 
		driver.findElement(By.id("allSearchField")).sendKeys(arg1);
		Thread.sleep(4000);
	    
	}

	@When("^Adv-Search-Or  = \"([^\"]*)\"$")
	public void adv_Search_Or(String arg1) throws Exception {
	    
		driver.findElement(By.id("orSearchField")).clear();
		driver.findElement(By.id("orSearchField")).sendKeys(arg1);
		Thread.sleep(4000); 
	}

	@When("^Adv-Search-Not = \"([^\"]*)\"$")
	public void adv_Search_Not(String arg1) throws Exception {
	    
		driver.findElement(By.id("notSearchField")).clear();
		driver.findElement(By.id("notSearchField")).sendKeys(arg1);
		Thread.sleep(4000); 
	}

	@When("^Month = \"([^\"]*)\" and Year = \"([^\"]*)\" and Date = \"([^\"]*)\" and Select On Date$")
	public void month_and_Year_and_Date_and_Select_On_Date(String arg1, String arg2, String arg3) throws Exception {
	    
		driver.findElement(By.xpath("//label[text()='Is on date']")).click();
		Thread.sleep(1000);  
		driver.findElement(By.xpath("//input[@id='on-date-value']")).click();
		Thread.sleep(1000); 
		
		dateon = "//div[@id='on-date-value_root']//select[@class='picker__select--";
		dateendpart = " browser-default']";
		
		datemonthxpath = dateon + "month" + dateendpart;
		WebElement Month_Dropdown = driver.findElement(By.xpath(datemonthxpath));
		Thread.sleep(2000);
		Select mondropdown = new Select(Month_Dropdown);
		mondropdown.selectByVisibleText(arg1);
		Thread.sleep(2000);
		
		dateyearxpath = dateon + "year" + dateendpart;
		WebElement Year_Dropdown = driver.findElement(By.xpath(dateyearxpath));
		Thread.sleep(2000);
		Select yeardropdown = new Select(Year_Dropdown);
		yeardropdown.selectByVisibleText(arg2);
		Thread.sleep(2000);
		
		datedayxpath = "//div[@id='on-date-value_root']//table[@id='on-date-value_table']//div[text()='" + arg3 + "']";
		driver.findElement(By.xpath(datedayxpath)).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//div[@id= 'on-date-value_root']//button[@class='btn-flat picker__close waves-effect' and text()='Ok']")).click();
		Thread.sleep(3000);
	}

	@Then("^Click Adv search button$")
	public void click_Adv_search_button() throws Exception {
	    
		driver.findElement(By.xpath("//a[text()='Search']")).click();
		Thread.sleep(4000);
	}

	@Then("^Combination Check$")
	public void combination_Check() throws Exception {
	    
	    
	}

	@Then("^Date Picker$")
	public void date_Picker() throws Exception {
	    
	    
	}

	@Then("^Date Validation Manually entering the date$")
	public void date_Validation_Manually_entering_the_date() throws Exception {
	    
	    
	}

	@Then("^Clear-Search-Filter$")
	public void clear_Search_Filter() throws Exception {
	    
		driver.findElement(By.xpath("//a[text()='Clear all']")).click();
		Thread.sleep(4000);
	    
	}

	@Then("^Filter-Video-Content$")
	public void filter_Video_Content() throws Exception {
	    
		driver.findElement(By.xpath("//label[@for='videos' and @class='label-alter']")).click();
		Thread.sleep(4000);
	    
	}

	@Then("^Select-all-content$")
	public void select_all_content() throws Exception {
	    
		driver.findElement(By.xpath("//label[text()='Select all']")).click();
		Thread.sleep(2000);
	    //DeSelect All videos resulted from Search query
	    driver.findElement(By.xpath("//label[text()='Select all']")).click();
	    Thread.sleep(2000);
	    
	    
	}

	@Then("^Filter-Photo-Content$")
	public void filter_Photo_Content() throws Exception {
	    
		driver.findElement(By.xpath("//label[@for='photo' and @class='label-alter']")).click();
		Thread.sleep(4000);
		
	}

	@Then("^Select-Timeline$")
	public void select_Timeline() throws Exception {
	    
		driver.findElement(By.xpath("//label[@for='2010s' and @class='label-alter']")).click();
		Thread.sleep(4000);
	    
	}

	@Then("^Cross-Verify-Timeline-Filter$")
	public void cross_Verify_Timeline_Filter() throws Exception {
	    
		driver.findElement(By.xpath("//label[@for='1990s' and @class='label-alter']")).click();
	    Thread.sleep(4000);
		
	}

	@When("^Select-Result-Pagewise$")
	public void select_Result_Pagewise() throws Exception {
	    
		driver.findElement(By.xpath("//i[text()='chevron_right']")).click();
		Thread.sleep(4000);
		
		driver.findElement(By.xpath("//i[text()='chevron_left']")).click();
		Thread.sleep(4000);
	}

	@Then("^clear-Selection$")
	public void clear_Selection() throws Exception {
	    
		driver.findElement(By.xpath("//div[@class='innerpage-search']//input[@class='select-dropdown']")).click();
  	    Thread.sleep(4000);
		  	    
  	    driver.findElement(By.xpath("//li//span[text()='All content']")).click();
  	    Thread.sleep(4000);
	    
	}

	@Then("^Select-Keyword = \"([^\"]*)\" and Click Keyword search button$")
	public void select_Keyword_and_Click_Keyword_search_button(String arg1) throws Exception {
	    
		driver.findElement(By.xpath("//h5[text()='Keywords']")).click();
		Thread.sleep(4000);
		
		keyname = arg1;
		keyxpath = "//a[text()='"+ keyname +"']";
		
		driver.findElement(By.xpath(keyxpath)).click();
		Thread.sleep(4000);
	}

	@When("^sorttype = \"([^\"]*)\" and Select-Sortmode$")
	public void sorttype_and_Select_Sortmode(String arg1) throws Exception {
	    
		driver.findElement(By.xpath("//div[@class='select-wrapper order-by initialized']//input[@class='select-dropdown']")).click();
		Thread.sleep(4000);
		
		sorttype = arg1;
		sortxpath = "//li//span[text()='"+ sorttype +"']";
		
		driver.findElement(By.xpath(sortxpath)).click();
		Thread.sleep(5000);
	}

	@When("^Count = \"([^\"]*)\" and Select-moreRecs-toView$")
	public void count_and_Select_moreRecs_toView(String arg1) throws Exception {
	    
	    
		driver.findElement(By.xpath("//div[@class='select-wrapper per-page-value initialized']//input[@class='select-dropdown']")).click();
		Thread.sleep(5000);
		
		reccount = arg1;
		recxpath = "//li//span[text()='"+ reccount +"']";
		driver.findElement(By.xpath(recxpath)).click();
		Thread.sleep(5000);
	}

	@When("^alligntype = \"([^\"]*)\" and Select-Allignment$")
	public void alligntype_and_Select_Allignment(String arg1) throws Exception {
	    
		allignname = arg1;
		allignxpath = "//i[text()='"+ allignname +"']";
		
		driver.findElement(By.xpath(allignxpath)).click();
		Thread.sleep(4000);   
		
	}

	@When("^FMonth = \"([^\"]*)\" and FYear = \"([^\"]*)\" and FDate = \"([^\"]*)\" and Select From Date$")
	public void fmonth_and_FYear_and_FDate_and_Select_From_Date(String arg1, String arg2, String arg3) throws Exception {
	    
		driver.findElement(By.xpath("//label[text()='Between']")).click();
		Thread.sleep(2000);  
		
		driver.findElement(By.xpath("//input[@id='between-date-from']")).click();
		Thread.sleep(2000); 
		
		dateon = "//div[@id='between-date-from_root']//select[@class='picker__select--";
		dateendpart = " browser-default']";
		
		datemonthxpath = dateon + "month" + dateendpart;
		WebElement Month_Dropdown = driver.findElement(By.xpath(datemonthxpath));
		Thread.sleep(3000);
		Select mondropdown = new Select(Month_Dropdown);
		mondropdown.selectByVisibleText(arg1);
		Thread.sleep(3000);
		
		dateyearxpath = dateon + "year" + dateendpart;
		WebElement Year_Dropdown = driver.findElement(By.xpath(dateyearxpath));
		Thread.sleep(3000);
		Select yeardropdown = new Select(Year_Dropdown);
		yeardropdown.selectByVisibleText(arg2);
		Thread.sleep(3000);
		
		datedayxpath = "//div[@id='between-date-from_root']//table[@id='between-date-from_table']//div[text()='" + arg3 + "']";
		driver.findElement(By.xpath(datedayxpath)).click();
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//div[@id= 'between-date-from_root']//button[@class='btn-flat picker__close waves-effect' and text()='Ok']")).click();
		Thread.sleep(4000);
	    
	}

	@When("^TMonth = \"([^\"]*)\" and TYear = \"([^\"]*)\" and TDate = \"([^\"]*)\" and Select To Date$")
	public void tmonth_and_TYear_and_TDate_and_Select_To_Date(String arg1, String arg2, String arg3) throws Exception {
	    
		driver.findElement(By.xpath("//input[@id='between-date-to']")).click();
		Thread.sleep(4000); 
		
		dateon = "//div[@id='between-date-to_root']//select[@class='picker__select--";
		dateendpart = " browser-default']";
		
		datemonthxpath = dateon + "month" + dateendpart;
		WebElement Month_Dropdown = driver.findElement(By.xpath(datemonthxpath));
		Thread.sleep(3000);
		Select mondropdown = new Select(Month_Dropdown);
		mondropdown.selectByVisibleText(arg1);
		Thread.sleep(3000);
		
		dateyearxpath = dateon + "year" + dateendpart;
		WebElement Year_Dropdown = driver.findElement(By.xpath(dateyearxpath));
		Thread.sleep(3000);
		Select yeardropdown = new Select(Year_Dropdown);
		yeardropdown.selectByVisibleText(arg2);
		Thread.sleep(3000);
		
		datedayxpath = "//div[@id='between-date-to_root']//table[@id='between-date-to_table']//div[text()='" + arg3 + "']";
		//driver.findElement(By.xpath(datedayxpath)).click();
		driver.findElement(By.xpath("//div[@id='between-date-to_root']//table[@id='between-date-to_table']//div[text()='4']")).click();
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//div[@id= 'between-date-to_root']//button[@class='btn-flat picker__close waves-effect' and text()='Ok']")).click();
		Thread.sleep(4000);
	}

	@When("^Select Item$")
	public void select_Item() throws Exception {
	    
		driver.findElement(By.xpath("//div[@id='thumbnail-view']//div[@class='card-content card-content-alter2']")).click();
		Thread.sleep(4000);
	    
	}

	@Given("^Back to Search Page$")
	public void back_to_Search_Page() throws Exception {
	    
		//driver.findElement(By.xpath("//a[@id='backtoDiv']")).click();
		//driver.findElement(By.id("backtoDiv")).click();
		
		 WebElement Back = driver.findElement(By.xpath("//a[text()='Back']"));
		 ((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+Back.getLocation().x+")");
		 Back.click();
		Thread.sleep(4000);
		
	}

	@Then("^Add to Wishlist$")
	public void add_to_Wishlist() throws Exception {
		//driver.findElement(By.xpath("//a[@id='srch_relaItems']")).click();
		driver.findElement(By.xpath("//span[@class='wishlist-items']//i[text()='create_new_folder']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//label[text()='test1 ']")).click();	
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@id='save-clipbins']")).click();
		Thread.sleep(4000);
	    
	}

	@Then("^Add to Cart$")
	public void add_to_Cart() throws Exception {
	
		//driver.findElement(By.xpath("//i[text()='add_shopping_cart']")).click();
		driver.findElement(By.xpath("//div[@class='card-action card-action-search']//a[@class='cart-submit-icon']//i[text()='add_shopping_cart']")).click();
		Thread.sleep(4000);
		
		driver.quit();
	    
	}
	
	
	
	
}
